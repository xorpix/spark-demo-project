package com.demo;

import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.IOException;
import java.util.LinkedList;

public class ModelLearning {
    private LogisticRegression lr;
    private LogisticRegressionModel lrm;
    private DecisionTreeClassifier decisionTreeClassifier;
    DecisionTreeClassificationModel dt_model;
    private String[] cols;
    private String feature;
    private String label;
    private boolean modelIndicator;
    private boolean trainLRIsUsed = false;
    private boolean trainDTIsUsed = false;

    public void trainLogisticRegression(Dataset<Row> researchDataset, int maxIter, double regParam, double elasticNetParam) throws IOException {
        if(!trainLRIsUsed)
        {
            if (!label.isEmpty())
            {
                lr = new LogisticRegression()
                        .setLabelCol(label)
                        .setMaxIter(maxIter)
                        .setRegParam(regParam)
                        .setElasticNetParam(elasticNetParam);
                lrm = lr.fit(researchDataset);
            }
            else
            {
                throw new IOException("Label is empty");
            }
        }
        else
        {
            System.out.println("Logistic Regression model was already trained. Action aborted");
        }
    }

    Dataset<Row> fitDataset(Dataset<Row> trainDataset, Dataset<Row> testDataset) throws IOException {
        modelIndicator = false;
        Dataset<Row> res = null;
        HyperParametersLR hyperParametersLR = formBestLRHyperParameters(trainDataset, testDataset);
        HyperParametersDT hyperParametersDT = formBestDTHyperParameters(trainDataset,testDataset);
        trainLogisticRegression(trainDataset, hyperParametersLR.getMaxIterations(), hyperParametersLR.getRegParam(), hyperParametersLR.getElasticNp());
        trainDecisionTree(trainDataset, hyperParametersDT.getMaxDepth(), hyperParametersDT.getMaxBins(), hyperParametersDT.getImpurity());
        double rocCurveLR = evaluateModel(testDataset, 0);
        double rocCurveDT = evaluateModel(testDataset, 1);
        if (Double.compare(rocCurveDT, rocCurveLR) > 0)
        {
            res = predictDecisionTree(trainDataset);
            saveModelDT("/home/xorpix/spark-demo-project/src/main/resources/models/");
            modelIndicator = true;
        }
        else {
            res = predictLogisticRegression(trainDataset);
            saveModelLR("/home/xorpix/spark-demo-project/src/main/resources/models/");
        }
        return res;
    }

    public void trainDecisionTree(Dataset<Row> researchDataset, int maxDepth, int maxBins, String impurity) throws IOException {
        if(!trainDTIsUsed)
        {
            if (!label.isEmpty() && (impurity.equals("gini") || impurity.equals("entropy"))) {
                decisionTreeClassifier = new DecisionTreeClassifier()
                        .setLabelCol(label)
                        .setMaxBins(maxBins)
                        .setMaxDepth(maxDepth)
                        .setImpurity(impurity);
                dt_model = decisionTreeClassifier.fit(researchDataset);
            }
            if (label.isEmpty()) throw new IOException("Label is empty");
        }
        else
        {
            System.out.println("Decision Tree model was already trained. Action aborted");
        }
    }

    public Dataset<Row> predictDecisionTree(Dataset<Row> researchDataset)
    {
        return dt_model.transform(researchDataset);
    }


    public double evaluateModel(Dataset<Row> researchDataset, int decisionTreeIndicator) throws IOException {
        //predictions.select("y", "rawPrediction", "prediction", "probability");
        BinaryClassificationEvaluator evaluator = new BinaryClassificationEvaluator();
        if(!label.isEmpty()) {
            evaluator = new BinaryClassificationEvaluator().setLabelCol(label);
        }
        else
        {
            throw new IOException("Label is empty");
        }
        if(decisionTreeIndicator == 1)
        {
            return evaluator.evaluate(predictDecisionTree(researchDataset));
        }
        return evaluator.evaluate(predictLogisticRegression(researchDataset));
    }

    public void saveModelLR(String path) throws IOException
    {
        trainLRIsUsed = true;
        lr.write().overwrite().save(path);
    }

    public void loadModelLR(String path) throws IOException
    {
        lrm = LogisticRegressionModel.load(path);
    }

    public void saveModelDT(String path) throws IOException
    {
        trainDTIsUsed = true;
        decisionTreeClassifier.write().overwrite().save(path);
    }

    public void loadModelDT(String path) throws IOException
    {
        lrm = LogisticRegressionModel.load(path);
        decisionTreeClassifier = DecisionTreeClassifier.load(path);
    }

    public Dataset<Row> predictLogisticRegression(Dataset<Row> researchDataset) throws IOException {
        return lrm.transform(researchDataset);
    }

    public HyperParametersLR formBestLRHyperParameters(Dataset<Row> researchDatasetForTraining, Dataset<Row> resultDataset) throws IOException {
        double rocCurve = 0;
        double bestCurve = rocCurve;
        Dataset<Row> experiment;
        LinkedList<HyperParametersLR> hpLR = new LinkedList<HyperParametersLR>();
        int i;
        double regPar = 0.1;
        double elasticN = 0.5;
        HyperParametersLR bestHyperParamLR = null;
        for (i = 10; i <= 50; i++)
        {
            hpLR.add(new HyperParametersLR(i,regPar,elasticN));
        }
        i = 10;
        for (int p = 0; p <= 50; p++)
        {
            hpLR.add(new HyperParametersLR(i,regPar,elasticN));
            regPar += 0.1;
        }
        regPar = 0.1;
        for (int p = 0; p <= 90; p++)
        {
            if(Double.compare(elasticN, 1.0)>0){ hpLR.add(new HyperParametersLR(i, regPar, 1.0)); break;};
            hpLR.add(new HyperParametersLR(i,regPar,elasticN));
            elasticN += 0.01;
        }
        for (HyperParametersLR hyperParam: hpLR) {
            System.out.println("Iter: "+hyperParam.getMaxIterations() + "  RegParam: "+hyperParam.getRegParam()+"  ElasticNP: "+hyperParam.getElasticNp());
        }
        for (HyperParametersLR hyperParam: hpLR)
        {
            trainLogisticRegression(researchDatasetForTraining, hyperParam.getMaxIterations(), hyperParam.getRegParam(), hyperParam.getElasticNp());
            experiment = predictLogisticRegression(resultDataset);
            //experiment.show(false);
            bestCurve = evaluateModel(experiment.drop("prediction", "rawPrediction", "probability"),0);
            if(Double.compare(bestCurve, rocCurve)>0)
            {
                bestHyperParamLR = hyperParam;
                rocCurve = bestCurve;
            }
        }
        return bestHyperParamLR;
    }

    HyperParametersDT formBestDTHyperParameters(Dataset<Row> researchDatasetForTraining, Dataset<Row> resultDataset) throws IOException {
        double rocCurve = 0;
        double bestCurve = rocCurve;
        int maxD = 1;
        int maxB = 5;
        LinkedList<HyperParametersDT> hpDT = new LinkedList<HyperParametersDT>();
        HyperParametersDT bestDTHyperParameters = null;
        Dataset<Row> experiment;
        for(maxD = 1; maxD <= 30; maxD++)
        {
            hpDT.add(new HyperParametersDT("entropy",maxD, maxB));
            hpDT.add(new HyperParametersDT("gini",maxD, maxB));
        }
        maxD = 3;
        for(maxB = 3; maxB <= 100; maxB++)
        {
            hpDT.add(new HyperParametersDT("entropy",maxD,maxB));
            hpDT.add(new HyperParametersDT("gini",maxD,maxB));
        }
        for (HyperParametersDT hyperParam: hpDT) {
            trainDecisionTree(researchDatasetForTraining, hyperParam.getMaxDepth(),hyperParam.getMaxBins(),hyperParam.getImpurity());
            experiment = predictDecisionTree(resultDataset);
            bestCurve = evaluateModel(experiment.drop("prediction", "rawPrediction", "probability"),1);
            if(Double.compare(bestCurve, rocCurve)>0)
            {
                bestDTHyperParameters = hyperParam;
                rocCurve = bestCurve;
            }
        }
        return bestDTHyperParameters;
    }

    public String[] getCols() {
        return cols;
    }

    public String getFeature() {
        return feature;
    }

    public String getLabel() {
        return label;
    }

    public void setCols(String[] cols) {
        this.cols = cols;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isModelIndicator() {
        return modelIndicator;
    }
}
