package com.demo;

import java.util.Objects;

public class HyperParametersDT {
    private final String impurity;
    private final int maxDepth;
    private final int maxBins;

    public HyperParametersDT(String impurity, int maxDepth, int maxBins) {
        if(Objects.equals(impurity, "gini") || Objects.equals(impurity, "entropy")) {
            this.impurity = impurity;
        }
        else
        {
            System.out.println("Impurity contained inappropriate value, so it was set to default {gini}");
            this.impurity = "gini";
        }
        this.maxDepth = maxDepth;
        this.maxBins = maxBins;
    }

    public String getImpurity() {
        return impurity;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public int getMaxBins() {
        return maxBins;
    }
}
