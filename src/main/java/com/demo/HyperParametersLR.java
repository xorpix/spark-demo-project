package com.demo;

public class HyperParametersLR {
    private final int maxIterations;
    private final double regParam;
    private final double elasticNp;

    public HyperParametersLR(int maxIterations, double regParam, double elasticNp) {
        this.maxIterations = maxIterations;
        this.regParam = regParam;
        this.elasticNp = elasticNp;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public double getRegParam() {
        return regParam;
    }

    public double getElasticNp() {
        return elasticNp;
    }
}
