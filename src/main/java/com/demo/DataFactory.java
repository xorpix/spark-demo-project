package com.demo;

import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.List;

public class DataFactory {

    private final SparkSession sparkSession;

    

    public DataFactory(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Row> getDataFromList(List<Row> listOfRows, StructType schema) {
        return sparkSession.createDataFrame(listOfRows, schema);
    }



    public Dataset<Row> getDataFromCsvFile(String filePath, StructType schema) {
        //TODO
        return sparkSession
                .read()
                .option("header", "true")
                .option("delimiter", ";")
                .schema(schema)
                .csv(filePath);
    }

    public Dataset<Row> getDataFromCsvFile(String filePath) {
        //TODO
        return sparkSession
                .read()
                .option("header", "true")
                .option("delimiter", ";")
                .csv(filePath);
    }
}
