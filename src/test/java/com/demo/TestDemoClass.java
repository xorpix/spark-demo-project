package com.demo;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.spark_project.guava.collect.ImmutableList;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;

class TestDemoClass implements Serializable {

    private SparkSession spark;
    public Dataset<Row> datasetInput;
    public Dataset<Row> datasetForPredict;
    private ModelLearning modelLearning;

    public void readAndPrepareDataset()
    {
            final String[] inputColNames = new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "y"};
            final StructType schema = new StructType(new StructField[]{
                    new StructField(inputColNames[0], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[1], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[2], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[3], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[4], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[5], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[6], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[7], DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField(inputColNames[8], DataTypes.DoubleType, false, Metadata.empty()),
            });
            final DataFactory dataFactory = new DataFactory(spark);
            URL res = getClass().getClassLoader().getResource("ClassificationInputData.csv");
            assert res != null;
            File file = null;
            try {
                file = Paths.get(res.toURI()).toFile();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        assert file != null;
        String absolutePath = file.getAbsolutePath();
            datasetInput = dataFactory.getDataFromCsvFile(absolutePath, schema);
    }

    public void readDatasetForPredict()
    {
        final String[] inputColNames = new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8"};
        final StructType schema = new StructType(new StructField[]{
                new StructField(inputColNames[0], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[1], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[2], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[3], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[4], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[5], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[6], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[7], DataTypes.DoubleType, false, Metadata.empty()),
        });
        final DataFactory dataFactory = new DataFactory(spark);
        URL res = getClass().getClassLoader().getResource("ClassificationInputDataForPredict1.csv");
        assert res != null;
        File file = null;
        try {
            file = Paths.get(res.toURI()).toFile();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        String absolutePath = file.getAbsolutePath();
        datasetForPredict = dataFactory.getDataFromCsvFile(absolutePath, schema);
    }

    @BeforeAll
    static void initSystem() {
        //System.setProperty("hadoop.home.dir", "c:/program_files/hadoop");
        Logger.getLogger("org").setLevel(Level.ERROR);
    }

    @Test
    void TestReadingCSV() throws URISyntaxException
    {
        readAndPrepareDataset();
        datasetInput.printSchema();
        datasetInput.show();
    }

    @BeforeEach
    void initSparkSession() {
        spark = SparkSession
                .builder()
                .appName("TestDemoClass")
                .master("local")
                .config("spark.driver.allowMultipleContexts", "false")
                .config("spark.sql.shuffle.partitions", "8")
                //.config("spark.testing.memory", "2147480000") // On x32 system
                .getOrCreate();

        spark.sparkContext().setLogLevel("ERROR");

    }
    public static Seq<String> convertListToSeq(List<String> inputList) {
        return JavaConverters.asScalaIteratorConverter(inputList.iterator()).asScala().toSeq();
    }
    @Test
    void testMethod() throws URISyntaxException {

        final String[] inputColNames = new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "y"};
        final List<Row> dataInput = ImmutableList.of(
                RowFactory.create(1, 25.5),
                RowFactory.create(2, -4.2),
                RowFactory.create(3, 3.8),
                RowFactory.create(4, 1.9),
                RowFactory.create(5, 7.0),
                RowFactory.create(7, 0.5),
                RowFactory.create(6, 10.5)
        );

        final StructType schema = new StructType(new StructField[]{
                new StructField(inputColNames[0], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[1], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[2], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[3], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[4], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[5], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[6], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[7], DataTypes.DoubleType, false, Metadata.empty()),
                new StructField(inputColNames[8], DataTypes.DoubleType, false, Metadata.empty()),
        });


        final DataFactory dataFactory = new DataFactory(spark);

        final Dataset<Row> datasetInput1 = dataFactory.getDataFromList(dataInput, schema).repartition(4);
        //datasetInput1.show();


        datasetInput1.createOrReplaceTempView("test_tbl");

        final double TH = 2.0;

//        final Dataset<Row> result1 = spark.sql(
//                "select " +
//                        "  id, " +
//                        "  value " +
//                        "from test_tbl " +
//                        "where value >= " + Double.toString(TH) + " " +
//                        "order by value desc"
//        );
//        result1.show();
        List<String> columns = Arrays.asList("x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "y");
        final Dataset<Row> result2 = datasetInput1.select("x1", "y")
                .where(col("y").geq(lit(TH)))
                .orderBy(desc("y"));
        //result2.show();

        final Dataset<Row> result3 = datasetInput1.selectExpr(convertListToSeq(columns))
                .where(col("y").geq(lit(TH)))
                .orderBy(desc("y"));
        //result3.show();

        //assertEquals(result1.count(), result2.count());

    }



    @Test
    void testLogisticRegressionModel () throws URISyntaxException, IOException {
        readAndPrepareDataset();
        datasetInput.show();
        String[] all_feature_columns = datasetInput.columns();
        String[] feature_columns = Arrays.copyOf(all_feature_columns, all_feature_columns.length - 1);
        VectorAssembler vectorAssembler = new VectorAssembler().setInputCols(feature_columns).setOutputCol("features");
        Dataset<Row> datasetInputTransformed = vectorAssembler.transform(datasetInput);
        datasetInputTransformed.show(false);
        Dataset<Row>[] splitSets = datasetInputTransformed.randomSplit(new double[]{0.7, 0.3});
        Dataset<Row> train = splitSets[0];
        Dataset<Row> test = splitSets[1];
        modelLearning = new ModelLearning();
        modelLearning.setLabel("y");
        HyperParametersLR bestHyperParametersLR = modelLearning.formBestLRHyperParameters(datasetInputTransformed,test);
        test.show(false);
        System.out.println("Best reg param: "+bestHyperParametersLR.getRegParam());
        System.out.println("Best elasticNP: "+bestHyperParametersLR.getElasticNp());
        System.out.println("Best iterCount: "+bestHyperParametersLR.getMaxIterations());
        System.out.println("Best AUROC: "+modelLearning.evaluateModel(test.drop("prediction", "rawPrediction", "probability"),0));
        readDatasetForPredict();
        modelLearning.setCols(new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8"});
        modelLearning.setFeature("features");
        VectorAssembler va;
        if(modelLearning.getCols().length != 0 && !modelLearning.getFeature().isEmpty()) {
            va = new VectorAssembler()
                    .setInputCols(modelLearning.getCols())
                    .setOutputCol(modelLearning.getFeature());
        }
        else
        {
            if(modelLearning.getCols().length == 0) {
                throw new IOException("Cols string is empty");
            }
            throw new IOException("Features string is empty");
        }
        Dataset<Row> beforeRes = va.transform(datasetForPredict);
        Dataset<Row> predict = modelLearning.predictLogisticRegression(beforeRes);
        datasetForPredict.show(false);
        predict.drop("features").withColumnRenamed("prediction", "y (predicted)").show(false);
        //LogisticRegressionModel ourLR = modelLearning.trainLogisticRegression(train);
    }

    @Test
    void testDecisionTreeModel() throws URISyntaxException, IOException
    {
        readAndPrepareDataset();
        datasetInput.show();
        String[] all_feature_columns = datasetInput.columns();
        String[] feature_columns = Arrays.copyOf(all_feature_columns, all_feature_columns.length - 1);
        VectorAssembler vectorAssembler = new VectorAssembler().setInputCols(feature_columns).setOutputCol("features");
        Dataset<Row> datasetInputTransformed = vectorAssembler.transform(datasetInput);
        datasetInputTransformed.show(false);
        Dataset<Row>[] splitSets = datasetInputTransformed.randomSplit(new double[]{0.7, 0.3});
        Dataset<Row> train = splitSets[0];
        Dataset<Row> test = splitSets[1];
        modelLearning = new ModelLearning();
        modelLearning.setLabel("y");
        HyperParametersDT hyperParametersDT = modelLearning.formBestDTHyperParameters(datasetInputTransformed,test);
        test.show(false);
        System.out.println("Best impurity: "+hyperParametersDT.getImpurity());
        System.out.println("Best max bins count: "+hyperParametersDT.getMaxBins());
        System.out.println("Best max depth: "+hyperParametersDT.getMaxDepth());
        System.out.println("Best AUROC: "+modelLearning.evaluateModel(test.drop("prediction", "rawPrediction", "probability"),1));

        //modelLearning.trainDecisionTree(datasetInputTransformed, 5, 32);
        modelLearning.loadModelDT("/home/xorpix/spark-demo-project/src/main/resources/models/");
        Dataset<Row> result = modelLearning.predictDecisionTree(test);
        result.show(false);
    }

    @Test
    void testUniversalFitMethod() throws IOException {
        readAndPrepareDataset();
        datasetInput.show();
        String[] all_feature_columns = datasetInput.columns();
        String[] feature_columns = Arrays.copyOf(all_feature_columns, all_feature_columns.length - 1);
        VectorAssembler vectorAssembler = new VectorAssembler().setInputCols(feature_columns).setOutputCol("features");
        Dataset<Row> datasetInputTransformed = vectorAssembler.transform(datasetInput);
        datasetInputTransformed.show(false);
        Dataset<Row>[] splitSets = datasetInputTransformed.randomSplit(new double[]{0.7, 0.3});
        Dataset<Row> train = splitSets[0];
        Dataset<Row> test = splitSets[1];
        modelLearning = new ModelLearning();
        modelLearning.setLabel("y");
        Dataset<Row> result_set = modelLearning.fitDataset(datasetInputTransformed, test);
        result_set.show(false);
        if(modelLearning.isModelIndicator())
        {
            double rocDT = modelLearning.evaluateModel(result_set.drop("prediction", "rawPrediction", "probability"),1);
            System.out.println("Decision Tree was more effective with current dataset, so it was used\nRoc: "+rocDT);
        }
        else
        {
            double rocLR = modelLearning.evaluateModel(result_set.drop("prediction", "rawPrediction", "probability"),0);
            System.out.println("Logistic Regression was more effective with current dataset, so it was used\nRoc: "+rocLR);
        }
    }

}